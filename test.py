import torch
from get_sentiment import VoiceClassifier, get_features_from_wav

model = VoiceClassifier()
model.load_state_dict(torch.load('checkpoints/checkpoint_FE_predue_8k_v04.pt', map_location=torch.device('cuda')))
model.eval()
model.cuda()

X = []
for wavfile in ['CC_CPC_10082_0.wav', 'CC_CPC_10082_0.wav', 'CC_CPC_10082_0.wav']:
    x = get_features_from_wav(wavfile, max_len=600)
    X.append(x)
X = torch.stack(X).cuda()

result = torch.argmax(model(X), axis=1).cpu().numpy()