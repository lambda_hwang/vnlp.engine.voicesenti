import numpy as np
import torch
from torch import nn
from torch.nn import functional as F
import soundfile as sf
import librosa
from python_speech_features import mfcc
import gc

def get_mfcc(path, resampled_rate=8000):
    sig, rate = sf.read(path)
    if (len(sig)==0):
        return np.array([[0]*13])
    else:
        if rate!=resampled_rate:
            sig = librosa.resample(sig, rate, resampled_rate)
        return mfcc(sig, rate, winlen=0.03, winstep=0.015, numcep=13)

def delta(a):
    if len(a) > 4:
        result = []
        for i in range(2, len(a) - 2):
            value = (a[i+1] - a[i-1] + 2*(a[i+2] - a[i-2])) * 0.1
            result.append(value)
        return np.pad(result, 2, 'constant', constant_values=0)
    else:
        return np.zeros(len(a))

def get_features_from_wav(filedir, max_len):
    mfcc0 = get_mfcc(filedir)
    mfcc1 = np.apply_along_axis(delta, 0, mfcc0)
    mfcc2 = np.apply_along_axis(delta, 0, mfcc1)

    mfcc_feat = np.concatenate((mfcc0, mfcc1, mfcc2), axis=1)
    num_frames, frame_size = mfcc_feat.shape[0] , mfcc_feat.shape[1]

    max_features = frame_size * max_len
    features = torch.from_numpy(mfcc_feat.reshape(-1))
    features = nn.ConstantPad1d((0, max_features - len(features)), 0)(features)
    features = features.reshape(max_len,frame_size)
    features = features.view(int(max_len / 2),features.size(1) * 2)
    features = torch.split(features, frame_size, dim=1) # it returns a tuple
    features = (features[0] + features[1]) / 2.0
    return features.float()

class VoiceClassifier(nn.Module):
    
    def __init__(self, audio_dim = 39, max_len = 300, 
                 num_kernels = [200,100,50,25],
                 kernel_sizes = [(7,39),(5,100),(3,50),(2,25)]):
        
        super(VoiceClassifier,self).__init__()
        self.convs = nn.ModuleList([])
        self.norms = nn.ModuleList([])
        
        for i in range(0,len(kernel_sizes)):
            self.convs.append(nn.Conv2d(in_channels = 1,
                                        out_channels = num_kernels[i],
                                        kernel_size = kernel_sizes[i],
                                        stride = 2))
            self.norms.append(nn.BatchNorm2d(num_kernels[i]))
        
        self.dropout = nn.Dropout(0.2)
        self.fc1 = nn.Linear(204, 2)  # Determine in runtime
        self.softmax = nn.Softmax(dim=1)

        gc.collect()
        
    def forward(self, x):
        # x = [N, max_len, 512]
        
        for conv in self.convs:
            x = x.unsqueeze(1) # x = [N, num_channels, max_len, 512]
            x = F.relu(conv(x)) # x = [N, num_kernel, K, 1]
            x = x.squeeze(3) # x = [N, num_kernel, K]
            x = x.permute(0,2,1) # x = [N, K, num_kernel]
            x = F.max_pool1d(x, 2) # x = [N, K, num_kernel / 2] # x = F.avg_pool1d(x, 2) 
            x = self.dropout(x)
        
        x = x.reshape(-1, x.size(1) * x.size(2))
        
        logit = self.fc1(x)
        logit = self.softmax(logit)

        gc.collect()
        
        return logit
